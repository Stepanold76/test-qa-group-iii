package suites;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import qaii.cases.DvdRentalTest;
import qaii.cases.PetStoryTest;
import qaii.cases.SlotGameAllTest;
import qaii.cases.SlotGameTest;

/**
 * The type Qa automation engineer test task test suite.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        //PetStoryTest.class,
        //SlotGameTest.class,
        //SlotGameAllTest.class,
        DvdRentalTest.class

})
public class QaAutomationEngineerTestTaskTestSuite {

}
