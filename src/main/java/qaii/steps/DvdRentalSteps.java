package qaii.steps;

import net.thucydides.core.annotations.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import qaii.common.QaProjectCommon;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Dvd rental steps.
 */
public class DvdRentalSteps {

    private QaProjectCommon common = QaProjectCommon.getInstance();
    private static final Logger log = LogManager.getLogger(DvdRentalSteps.class);
    private Connection connection = null;

    /**
     * Connection step test.
     */
    @Step
    public void connectionStepTest(){
           connection = common.PostgreSQLConnector();
            common.DbConnectionClose(connection);
    }

    /**
     * Read data from db using sql.
     *
     * @param sql     the sql
     * @param columns the columns
     */
    @Step("Show data from DB using SQL {0}")
    public void readDataFromDbUsingSql(String sql, String [] columns){
        connection = common.PostgreSQLConnector();
        common.readDataFromDataBase(connection,sql,columns);
        common.DbConnectionClose(connection);
    }

    /**
     * Create data file from db.
     *
     * @param sql      the sql
     * @param filePath the file path
     */
    @Step("Save data from DB using SQL {0} in the file {1}")
    public void createDataFileFromDB(String sql, String filePath){
        connection = common.PostgreSQLConnector();
        common.readDataFromDataBaseAndSaveInCsv(connection, sql, filePath);
        common.DbConnectionClose(connection);
    }

    @Step("Return number of rows from DB using SQL {0}")
    public String  numberOfRow(String sql, String columnName) throws SQLException {

        String result = "";

        connection = common.PostgreSQLConnector();

        try (ResultSet rs = connection.createStatement().executeQuery(sql)) {
            if (rs == null) {
                result = "Result set is null";
            }
            else {
                while (rs.next()){
                    if(rs.findColumn(columnName)< 1) {
                        result = "Result is not found";
                    }
                    else {
                        result = rs.getString(columnName);
                    }
                }

            }

            } catch(
                    SQLException throwables){
                log.error("Error in SQL {}", throwables);

            }

        common.DbConnectionClose(connection);
        return result;
    }

}
