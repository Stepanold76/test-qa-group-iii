package qaii.steps;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import qaii.common.QaProjectCommon;

import net.thucydides.core.annotations.Step;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Slot game steps.
 */
public class SlotGameSteps {

    private static final Logger log = LogManager.getLogger(SlotGameSteps.class);
    private QaProjectCommon common = QaProjectCommon.getInstance();
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private int errorCounter = 0;


    /**
     * Open chrome.
     */
    @Step("Open Chrome and navigate to base url")
    public void openChrome()
    {
        common.openBrowser();
    }

    /**
     * Verify game boolean.
     *
     * @param testData         the test data
     * @param winAmount        the win amount
     * @param winCombination   the win combination
     * @param selector         the selector
     * @param blinkCombination the blink combination
     * @return the boolean
     * @throws Exception the exception
     */
    @Step ( "Verify game with combination {2}")
    public boolean verifyGame(String testData,
                              int winAmount,
                              String winCombination,
                              String selector,
                              String blinkCombination
    ) throws Exception {


        char [] winNumber = testData.toCharArray();
        char [] blinkMe =  blinkCombination.toCharArray();
        Map<String,Integer> balance = new HashMap<String,Integer>();
        balance.put("initialBalance",common.getBalance());

        log.info("Initial balance is {}", balance.get("initialBalance"));
        common.insertTestData(testData);
        common.runGame();

        if(winAmount > 0 ) {
            if(!common.checkWinInformation(Integer.toString(winAmount)) ) errorCounter++;
            if(!common.checkWinCombinationIlluminated(winCombination,selector)) errorCounter++ ;
            
        }

        for (int i=0; i<5; i++) {
            String number = Character.toString(testData.charAt(i));
            String line = Integer.toString(++i);
            if (winAmount > 0 && !number.equals("0"))
                if(!common.checkWinCombinationByTab( number, line, "Y")) errorCounter++;
                else
                if(!common.checkWinCombinationByTab( number, line, "N")) errorCounter++;
        }

        balance.put("finalBalance",common.getBalance());

        log.info("Final balance is {}", balance.get("finalBalance"));
        if (!(( (balance.get("initialBalance")) - 1 + winAmount) == balance.get("finalBalance"))) {
            log.error("Result is not correct. Expected result {}, Actual result {}", ((balance.get("initialBalance")- 1 + winAmount)), balance.get("finalBalance"));
            acceptNextAlert = false;
        }
        if (acceptNextAlert)
            return true;
        else
            return false;

    }


}
