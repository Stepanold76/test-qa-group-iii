package qaii.cases;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import qaii.steps.DvdRentalSteps;

import java.sql.SQLException;


/**
 * The type Dvd rental test.
 */
@RunWith(SerenityRunner.class)

public class DvdRentalTest {

    private static final Logger log = LogManager.getLogger(DvdRentalTest.class);


    /**
     * The Steps.
     */
    @Steps
    DvdRentalSteps steps;

    /**
     * Database connection test.
     */
    @Test
    public void databaseConnectionTest(){
         steps.connectionStepTest();

     }

    /**
     * Read data from actor.
     */
    @Test
    public void readDataFromActor(){
        steps.readDataFromDbUsingSql(
                "SELECT * FROM ACTOR WHERE first_name LIKE 'A%'" ,
                new String[] { "actor_id","first_name", "last_name", "last_update"}
        );

    }

    /**
     * Read data from film.
     */
    @Test
    public void readDataFromFilm(){
        steps.readDataFromDbUsingSql(
                "SELECT * FROM FILM WHERE title LIKE 'A%'" ,
                new String[] { "film_id","title", "description", "release_year"}
        );

    }

    /**
     * Read data from film and save in file.
     */
    @Test
    public void readDataFromFilmAndSaveInFile(){
        steps.createDataFileFromDB(
                "SELECT * FROM FILM WHERE title LIKE 'A%'" ,
                "./src/test/resources/FilmData.csv");
    }

    /**
     * Number of films which name started from a.
     *
     * @throws SQLException the sql exception
     */
    @Test
    public void numberOfFilmsWhichNameStartedFromA() throws SQLException {

        String result = steps.numberOfRow("SELECT COUNT (*) FROM FILM WHERE title LIKE 'A%'" , "COUNT");

        log.info("Numbers of films is {}", result);

        Assert.assertEquals("46", result );

    }


    /**
     * Find film by name.
     *
     * @throws SQLException the sql exception
     */
    @Test
    public void findFilmByName() throws SQLException {

        String result = steps.numberOfRow("SELECT * FROM FILM WHERE title = 'Ali Forever' LIMIT 1" , "TITLE");

        log.info("Film Title is {}", result);

        Assert.assertEquals("Ali Forever", result );

        }
}
