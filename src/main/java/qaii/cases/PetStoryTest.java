package qaii.cases;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import qaii.common.QaProjectCommon;
import qaii.steps.PetStorySteps;


/**
 * The type Pet story test.
 */
@RunWith(SerenityRunner.class)

public class PetStoryTest {

    private QaProjectCommon common = QaProjectCommon.getInstance();
    private StringBuffer verificationErrors = new StringBuffer();
    private static final Logger log = LogManager.getLogger(PetStoryTest.class);


    /**
     * The Pet steps.
     */
    @Steps
    PetStorySteps petSteps;

    /**
     * Full circle test.
     */
    @Test
     public void fullCircleTest(){
         petSteps.deletePetFromStore("17920183512345");
         addPetToTheShop();
         apiReturnCorrectPetInformation("dog",
                 23,
                 "Winipuh",
                 0,
                 "tag_name",
                 "available");
         updatePetInTheShop();
         apiReturnCorrectPetInformation(
                 "dogs",
                 25,
                 "Winipuhhh",
                 2,
                 "tag_name1",
                 "sold");
         removePetFromShop ();
     }


    /**
     * Add pet to the shop.
     */
    public void addPetToTheShop (){
        petSteps.creteNewPet(
                "17920183512345",
                23,
                "dog",
                "Winipuh",
                new String[]{"url"},
                0,
               "tag_name",
               "available"
        );
        petSteps.searchIsExecutedSuccessfully();
    }


    /**
     * Update pet in the shop.
     */
    public void updatePetInTheShop (){
        petSteps.creteNewPet(
                "17920183512345",
                25,
                "dogs",
                "Winipuhhh",
                new String[]{"url1"},
                2,
                "tag_name1",
                "sold"
        );
        petSteps.searchIsExecutedSuccessfully();
    }


    /**
     * Remove pet from shop.
     */
    public void removePetFromShop (){
        petSteps.deletePetFromStore("17920183512345");
        petSteps.searchIsExecutedSuccessfully();
        petSteps.searchPetById("17920183512345");
        petSteps.searchIsResourceNotFound();
    }


    /**
     * Api return correct pet information.
     *
     * @param categoryName the category name
     * @param categoryId   the category id
     * @param name         the name
     * @param tagId        the tag id
     * @param tagsName     the tags name
     * @param status       the status
     */
    public void apiReturnCorrectPetInformation (
            String categoryName,
            int categoryId,
            String name,
            int tagId,
            String tagsName,
            String status)
    {
        petSteps.searchPetById("17920183512345");
        petSteps.searchIsExecutedSuccessfully();
        petSteps.iShouldFindPetById(
                17920183512345L,
                categoryName,
                categoryId,
                name,
                tagId,
                tagsName,
                status);

    }
}
