package qaii.cases;

import com.codeborne.selenide.junit.TextReport;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import qaii.common.QaProjectCommon;
import qaii.steps.SlotGameSteps;

import static org.junit.Assert.assertTrue;

/**
 * The type Slot game test.
 */
@RunWith(SerenityRunner.class)

public class SlotGameTest {

    private QaProjectCommon common = QaProjectCommon.getInstance();
    private StringBuffer verificationErrors = new StringBuffer();
    private static final Logger log = LogManager.getLogger(SlotGameTest.class);

    /**
     * The Slot steps.
     */
    @Steps
    SlotGameSteps slotSteps;


    /**
     * The Report.
     */
    @Rule
    public TextReport report = new TextReport();

    /**
     * Sets up allure.
     */
    @Before
    public void setUpAllure() {
        SelenideLogger.addListener( "allure", new AllureSelenide());
        common.openBrowser();
    }


    /**
     * Slot game test case win combination 11100.
     *
     * @throws Exception the exception
     */
    @Test
    public void slotGameTestCaseWinCombination11100() throws Exception {

        slotSteps.verifyGame("11100",60, "1 + 1 + 1", "win111","YYYNN");

    }

    /**
     * Slot game test case win combination 44440.
     *
     * @throws Exception the exception
     */
    @Test
    public void slotGameTestCaseWinCombination44440() throws Exception {

        assertTrue(slotSteps.verifyGame("44440",320, "4 + 4 + 4 + 4", "win4444", "YYYYN"));


    }

    /**
     * Slot game test case win combination 22200.
     *
     * @throws Exception the exception
     */
    @Test
    public void slotGameTestCaseWinCombination22200() throws Exception {

        assertTrue(slotSteps.verifyGame("22200",120, "2 + 2 + 2", "win222", "YYYNN"));


    }

    /**
     * Read game information.
     */
    @Test
    public void readGameInformation() {

        String [] lineInfo = common.CSVReader("./src/test/resources/TestData.csv", 2);

        log.info(" Game information: win combination {} , win amount {} " ,lineInfo[2], lineInfo[1]);
    }

    /**
     * Crete game information file.
     */
    @Test
    public void creteGameInformationFile(){

             Object gameData [] = new Object[6] ;
             gameData[0] = new String[] {"testData", "winAmount", "winCombination", "selector", "blinkCombination"};
             gameData[1] = new String[] {"11100", "60", "1 + 1 + 1", "win111", "YYYNN"};
             gameData[2] = new String[] {"11110", "80", "1 + 1 + 1 + 1", "win1111", "YYYYN"};
             gameData[3] = new String[] {"22200", "120", "2 + 2 + 2", "win222", "YYYNN"};
             gameData[4] = new String[] {"22000", "0", "2 + 2", "win22", "NNNNN"};
             gameData[5] = new String[] {"44440","320","4 + 4 + 4 + 4","win4444","YYYYN"};

             common.writeDataToCvs("./src/test/resources/TestData1.csv",gameData);

    }


}
