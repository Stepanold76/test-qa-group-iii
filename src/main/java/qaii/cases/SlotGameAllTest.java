package qaii.cases;

import com.codeborne.selenide.junit.TextReport;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import lombok.AccessLevel;
import lombok.Setter;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import qaii.common.QaProjectCommon;
import qaii.steps.SlotGameSteps;

import static org.junit.Assert.assertTrue;


/**
 * The type Slot game all test.
 */
@RunWith(SerenityParameterizedRunner.class)

@UseTestDataFrom( "./src/test/resources/TestData.csv"  )

public class SlotGameAllTest {

    private QaProjectCommon common = QaProjectCommon.getInstance();
    private StringBuffer verificationErrors = new StringBuffer();
    private static final Logger log = LogManager.getLogger(SlotGameAllTest.class);


    /**
     * The Test data.
     */
    @Setter(AccessLevel.PRIVATE)
    String  testData,
    /**
     * The Win amount.
     */
    winAmount,
    /**
     * The Win combination.
     */
    winCombination,
    /**
     * The Selector.
     */
    selector,
    /**
     * The Blink combination.
     */
    blinkCombination;

    /**
     * The Slot steps.
     */
    @Steps
    SlotGameSteps slotSteps;

    /**
     * Qualifier string.
     *
     * @return the string
     */
// Display information in test output
    @Qualifier
    public String qualifier() {
        return  winCombination;
    }

    /**
     * The Report.
     */
    @Rule
    public TextReport report = new TextReport();

    /**
     * Sets up allure.
     */
    @Before
    public void setUpAllure() {
        SelenideLogger.addListener( "allure", new AllureSelenide());
    }


    /**
     * Slot game test case.
     *
     * @throws Exception the exception
     */
    @Test
    public void slotGameTestCase() throws Exception {

        slotSteps.openChrome();
        assertTrue(slotSteps.verifyGame(
                testData,
                Integer. valueOf(winAmount),
                winCombination,
                selector,
                blinkCombination));


    }



}
