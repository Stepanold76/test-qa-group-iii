package qaii.common;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import com.codeborne.selenide.Configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.openqa.selenium.chrome.ChromeOptions;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * The type Qa project common.
 */
public class QaProjectCommon {

    private static final QaProjectCommon INSTANCE = new QaProjectCommon();

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static QaProjectCommon getInstance() {
        return INSTANCE;
    }
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    private static final Logger log = LogManager.getLogger(QaProjectCommon.class);
    private int errorCounter = 0;
    private String POSTGRESQL_DB_URL =System.getProperty("postgresql.db.url");
    private String POSTGRESQL_DB_PASSWORD =System.getProperty("postgresql.password.url");
    private String POSTGRESQL_DB_USER =System.getProperty("postgresql.user.url");


    /**
     * Open browser.
     */
    public void openBrowser () {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("enable-automation");
        options.addArguments("--headless");
        options.addArguments("--window-size=1920,1080");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-extensions");
        options.addArguments("--dns-prefetch-disable");
        options.addArguments("--disable-gpu");


        Configuration.browser = "chrome";
        Configuration.timeout = 8000;
        Configuration.baseUrl = System.getProperty("base.url");
        Configuration.startMaximized = true;
        Configuration.headless = false;

        open(Configuration.baseUrl);
    }


    /**
     * Tear down.
     */
    public void tearDown()  {
        close();
    }

    /**
     * Get balance int.
     *
     * @return the int
     */
    public int getBalance(){
        try {
            return Integer.parseInt($("#balance-value").getAttribute("value"));
        } catch (NumberFormatException e) {
            log.error("Balance is not accessible, {} ", e  );
        }
        return 0;

    }

    /**
     * Run game.
     *
     * @throws InterruptedException the interrupted exception
     */
    public void runGame() throws InterruptedException {

        $("#spinButton").shouldBe(enabled).click();

    }

    /**
     * Insert test data.
     *
     * @param testData the test data
     */
    public void insertTestData(String testData){
      $("#testdata").clear();
       $("#testdata").sendKeys(testData);
    }

    /**
     * Check win information boolean.
     *
     * @param winAmount the win amount
     * @return the boolean
     * @throws InterruptedException the interrupted exception
     */
    public boolean checkWinInformation(String winAmount) throws InterruptedException {

        if($("#winbox").isDisplayed()) {

            try {
                assertEquals("Win " + winAmount + " coins", $("#winbox").getText());
            } catch (Error e) {
                log.error("Information about win is incorrect. Expected: Win {} coins. Actual: {}"
                        , winAmount, $("#winbox").getText());
                return false;
            }
        }
        else {
            log.error("Information about win amount is not displayed");
        }
        return true;

    }

    /**
     * Check win combination by tab boolean.
     *
     * @param winNumber the win number
     * @param tabNumber the tab number
     * @param blinkMe   the blink me
     * @return the boolean
     */
    public boolean checkWinCombinationByTab(String winNumber, String tabNumber, String blinkMe)  {

        String locator = "#reel" + tabNumber + "> div.notch.notch2" ;
        if (blinkMe.equals("Y"))locator = "#reel" + tabNumber + "> div.notch.notch2.blinkme";

        if($(locator).exists()) {

               $(locator).getText().equals(winNumber);

        }
        else {
            log.error("Information regarding win combination incorrectly displayed in {} tab.", tabNumber);
            return false;
        }


        return true;

    }

    /**
     * Check win combination illuminated boolean.
     *
     * @param winCombination the win combination
     * @param selector       the selector
     * @return the boolean
     * @throws InterruptedException the interrupted exception
     */
    public boolean checkWinCombinationIlluminated(String winCombination, String selector) throws InterruptedException {

        String locator = "tr." + selector + ".achievement.achievement.achievement.achievement.achievement > td";

            if($(locator).exists())
                $(locator).getText().equals(winCombination);
            else {
                log.error("Information regarding win combination incorrectly illuminated for combination: " + winCombination );
                return false;
            }

        return true;

    }

    /**
     * Csv reader string [ ].
     *
     * @param filePath   the file path
     * @param lineNumber the line number
     * @return the string [ ]
     */
    public String []  CSVReader(String filePath, int lineNumber){

            String[] line = null;
            CSVReader reader = null;
            try {
                reader = new CSVReader(new FileReader(filePath));

                int i = 0;
                while ((line = reader.readNext()) != null) {
                    if (i == lineNumber ) return line;
                    i++;
                }
            } catch (IOException e) {
               log.error("File not found, more info {}" , e );
            }
          log.info ("Line number {} not found", Integer.toString(lineNumber));
          return line;

        }

    /**
     * Write data to cvs.
     *
     * @param filePath  the file path
     * @param inputData the input data
     */
    public void writeDataToCvs(String filePath, Object [] inputData)
    {

        // first create file object for file placed at location
        // specified by filepath
        File file = new File(filePath);

        try {
            // create FileWriter object with file as parameter
            FileWriter outputfile = new FileWriter(file);

            // create CSVWriter object filewriter object as parameter
            CSVWriter writer = new CSVWriter(outputfile);

            // create a List which contains String array
           List<String[]> data = new ArrayList<String[]>();
           int lineNumber =0;
           while ( lineNumber < inputData.length) {
               data.add((String[]) inputData[lineNumber]);
               lineNumber++;
           }
            writer.writeAll(data);

            // closing writer connection
            writer.close();
        }
        catch (IOException e) {
           log.error("Can not crete file {}", e);

        }
    }

    /**
     * Postgre sql connector connection.
     *
     * @return the connection
     */
    public Connection PostgreSQLConnector()  {

        if (POSTGRESQL_DB_URL == null) {
            POSTGRESQL_DB_URL = "jdbc:postgresql://127.0.0.1:5555/dvdrental";
        }
        if (POSTGRESQL_DB_PASSWORD == null) {
            POSTGRESQL_DB_PASSWORD = "developer";
        }

        if (POSTGRESQL_DB_USER == null) {
            POSTGRESQL_DB_USER = "DEVELOPER";
        }

        System.out.println("-------- PostgreSQL "
                + "JDBC Connection Testing ------------");

        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();

        }

        System.out.println("PostgreSQL JDBC Driver Registered!");

        Connection connection = null;



        try {

            connection = DriverManager.getConnection(
                    POSTGRESQL_DB_URL, POSTGRESQL_DB_USER,
                    POSTGRESQL_DB_PASSWORD);


        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();


        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }

        return connection;
    }

    /**
     * Db connection close.
     *
     * @param connection the connection
     */
    public void DbConnectionClose( Connection connection){
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Read data from data base.
     *
     * @param connection the connection
     * @param sql        the sql
     * @param columns    the columns
     */
    public void readDataFromDataBase (Connection connection, String sql, String [] columns) {


        try (ResultSet rs = connection.createStatement().executeQuery(sql)) {

            int rsLength = 0;

            while (rs.next()) {
                rsLength++; }
            Object data [] = new Object[rsLength];
            log.info("Quantity of rows {}", rsLength);

            ResultSet rsDouble = connection.createStatement().executeQuery(sql);
            while (rsDouble.next()) {
                //Retrieve by column name
                // int id = rs.getInt("id");
                // int age = rs.getInt("age");

                //Display values
                //System.out.print("ID: " + id);
                //System.out.print(", Age: " + age);

               for (int i = 0; i < columns.length; i++)
                log.info(columns[i] + ": {}", rsDouble.getString(columns[i]));

            }
        } catch (SQLException throwables) {
            log.error("Error in SQL {}", throwables);
        }

    }

    /**
     * Read data from data base and save in csv.
     *
     * @param connection the connection
     * @param sql        the sql
     * @param filePath   the file path
     */
    public void readDataFromDataBaseAndSaveInCsv (Connection connection, String sql, String filePath) {


        try (ResultSet rs = connection.createStatement().executeQuery(sql)) {

            File file = new File(filePath);

            try {
                // create FileWriter object with file as parameter
                FileWriter outputFile = new FileWriter(file);

                // create CSVWriter object filewriter object as parameter
                CSVWriter writer = new CSVWriter(outputFile);

                writer.writeAll(rs, true);

                // closing writer connection
                writer.close();
            }
            catch (IOException e) {
                log.error("Can not crete file {}", e);

            }


        } catch (SQLException throwables) {
            log.error("Error in SQL {}", throwables);
        }

    }


}
